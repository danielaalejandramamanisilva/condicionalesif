/*la sentencia if

    if(condicion){
        Bloque de Instrucciones 1;
    }else{
        Bloque de instrucciones 2;
    }
*/
//ejercicio 1
//controlar si un digito ingresado es par (que sea mayor a 0)
#include <iostream>

using namespace std;

int main()
{
    /*int num, dato=5;
    cout <<"digite un numero: "<< endl;
    cin>> num;
    if(num == dato){
        cout<<"el numero es 5";
    }else{
        cout<<"el numero es diferente de 5";
    }*/
    //resolucion ejercicio 1
    /*int num;
    cout << "digite el numero" << endl;
    cin>>num;
    if (num<=0){
        cout<<"el numero es 0 o negativo"<<endl;
    }
    else {
        if (num%2!=0){
        cout<<"el numero no es par"<<endl;
    }else{
        cout<<"el numero es par"<<endl;
    }
    }*/
//Dos n�meros son �Aceptables� si la suma de ambos es menor que 10 o mayor que 20.
//Realice un algoritmo que dado 2 n�meros por teclado determinar si son �Aceptables� o
//�No Aceptables�.
    /*int n1,n2,suma=0;
    cout<<"digite 2 valores:";
    cin>>n1>>n2;
    suma=n1+n2;
    if(suma<10||suma>20){
        cout<<"la suma es: "<<suma<<endl;
        cout<<"Aceptable";
    }else{
        cout<<"la suma es: "<<suma<<endl;
        cout<<"No aceptable";
    }*/
//Realice un algoritmo para intercambiar dos variables (utilizando una variable auxiliar)
    int a,b,aux;
    cout<<"digite 2 valores para intercambiar";
    cin>>a>>b;
    aux=a;
    a=b;
    b=aux;
    cout<<"el valor intercambiado es: "<<a<<" "<<b;
    return 0;
}
